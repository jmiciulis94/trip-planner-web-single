import React from "react";

const HomePage = () => (
  <>
    <section className="hero">
      <div className="container">
        <div className="left-col">
          <p className="subhead">It's Nitty &amp; Gritty</p>
          <h1>A Task App That Doesn't Stink</h1>
          <div className="hero-cta">
            <a href="/#" className="primary-cta">
              Try for free
            </a>
            <a href="/#" className="watch-video-cta">
              <img src="../../../public/images/watch.svg" alt="Watch a video" />
              Watch a video
            </a>
          </div>
        </div>
        <img
          src="../../../public/images/illustration.svg"
          className="hero-img"
          alt="Illustration"
        />
      </div>
    </section>
  </>
);

export default HomePage;