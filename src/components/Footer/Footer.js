import React from "react";

const Footer = () => (
  <>
    <section className="features-section">
      <div className="container">
        <ul className="features-list">
          <li>Unlimited Tasks</li>
          <li>SMS Task Reminders</li>
          <li>Confetti Explosions upon Task Completions</li>
          <li>Social Media Announcements</li>
          <li>Real Time Collaboration</li>
          <li>Other awesome features</li>
        </ul>
      </div>
    </section>
  </>
);

export default Footer;