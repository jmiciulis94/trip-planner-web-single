import React from "react";

const Header = () => (
  <>
    <div className="navbar">
      <div className="container">
        <a className="logo" href="/#">Remember<span>That</span></a>

        <img id="mobile-cta" className="mobile-menu" src="../../../public/images/menu.svg" alt="Open Navigation"/>

        <nav>
          <img id="mobile-exit" className="mobile-menu-exit" src="../../../public/images/exit.svg" alt="Close Navigation"/>

          <ul className="primary-nav">
            <li className="current"><a href="/#">Home</a></li>
            <li><a href="/#">Features</a></li>
            <li><a href="/#">Pricing</a></li>
          </ul>

          <ul className="secondary-nav">
            <li><a href="/#">Contact</a></li>
            <li className="go-premium-cta"><a href="/#">Go Premium</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </>
);

export default Header;